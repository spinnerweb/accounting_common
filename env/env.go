package env

import "os"

// GetEnv Reads the environment variable and uses the default value as a fallback if it does not exist
func GetEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
